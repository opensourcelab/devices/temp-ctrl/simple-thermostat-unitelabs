#!/usr/bin/env python
"""Tests for `simple_thermostat_ul` package."""
# pylint: disable=redefined-outer-name
from simple_thermostat_ul import __version__
from simple_thermostat_ul.simple_thermostat_ul_interface import GreeterInterface
from simple_thermostat_ul.simple_thermostat_ul_impl import HelloWorld

def test_version():
    """Sample pytest test function."""
    assert __version__ == "0.0.1"

def test_GreeterInterface():
    """ testing the formal interface (GreeterInterface)
    """
    assert issubclass(HelloWorld, GreeterInterface)

def test_HelloWorld():
    """ Testing HelloWorld class
    """
    hw = HelloWorld()
    name = 'yvain'
    assert hw.greet_the_world(name) == f"Hello world, {name} !"

