.. highlight:: shell

============
Installation
============


Stable release
--------------

To install Simple Thermostat Ul, run this command in your terminal:

.. code-block:: console

    $ pip install simple_thermostat_ul

This is the preferred method to install Simple Thermostat Ul, as it will always install the most recent stable release.

If you don't have `pip`_ installed, this `Python installation guide`_ can guide
you through the process.

.. _pip: https://pip.pypa.io
.. _Python installation guide: http://docs.python-guide.org/en/latest/starting/installation/


From source
-----------

