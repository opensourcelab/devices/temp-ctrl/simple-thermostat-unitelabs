from . import create_app


def main():
    """Creates and starts the connector application"""
    app = create_app()
    app.start()


if __name__ == "__main__":
    main()
