import abc

from unitelabs import sila


class TemperatureControllerBase(sila.Feature, metaclass=abc.ABCMeta):
    """
    Example implementation of a minimum Feature. Provides a Greeting to the Client
    and a StartYear property, informing about the year the Server has been started.
    """

    def __init__(self):
        super().__init__(
            originator="org.silastandard",
            category="examples",
            version="1.0",
            maturity_level="Verified",
        )

    @abc.abstractmethod
    @sila.UnobservableCommand()
    @sila.Response(name="SetTemperature", description="The Temperature Setpoint = Target Temperature in Kelvin (K).")
    async def set_temperature(self, temperature: float) -> float:
        """
        Set the temperature to the given value.
        """

    @abc.abstractmethod
    @sila.ObservableCommand()
    @sila.Response(name="CurrentTemperature", description="The Current Temperature  in Kelvin (K).")
    async def get_temperature(self, temperature: float) -> float:
        """
        The current temperature.
        """

    @abc.abstractmethod
    @sila.UnobservableProperty()
    async def duration_running(self) -> int:
        """Returns the duration the SiLA Server is running."""
