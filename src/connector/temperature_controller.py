import datetime
import random

from .features.examples.temperature_controller import TemperatureControllerBase


class TemperatureController(TemperatureControllerBase):
    """
    Example implementation of a minimum Feature. Provides a Greeting to the Client
    and a StartYear property, informing about the year the Server has been started.
    """

    def __init__(self):
        super().__init__()
        self.__start_year = datetime.datetime.now().year

        self.max_temp = 0.0
        self.min_temp = 0.0

    async def set_temperature(self, temperature: float) -> float:
        return 42.0
    
    async def get_temperature(self, temperature: float) -> float:
        return self.determine_current_temperature()

    async def duration_running(self):
        return self.__start_year
    
    def determine_current_temperature(self):
        return float(random.randint(int(self.max_temp-5), int(self.max_temp+5)))
    

