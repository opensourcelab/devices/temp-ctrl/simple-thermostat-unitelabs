from unitelabs import Connector
from unitelabs.features.test.observable_command_test import ObservableCommandTest
from unitelabs.features.test.observable_property_test import ObservablePropertyTest
from unitelabs.features.test.unobservable_command_test import UnobservableCommandTest
from unitelabs.features.test.unobservable_property_test import UnobservablePropertyTest

from .greeting_provider import GreetingProvider
from .temperature_controller import TemperatureController


async def create_app():
    """Creates the connector application"""
    app = Connector(
        {
            "sila_server": {
                "name": "Simple Temperature Controller",
                "type": "Example",
                "description": "Simple Temperature Controller based on the UniteLabs SiLA Python Framework",
                "version": "0.1.0",
                "vendor_url": "https://gitlab.com/opensourcelab/",
            }
        }
    )

    app.register(ObservableCommandTest())
    app.register(ObservablePropertyTest())
    app.register(UnobservableCommandTest())
    app.register(UnobservablePropertyTest())

    app.register(GreetingProvider())
    app.register(TemperatureController())

    return app
