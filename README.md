# Simple Thermostat Ul

Simple Thermostat Implementation with UniteLabs SiLA framework

## Features

## Installation

    pip install simple_thermostat_ul --index-url https://gitlab.com/api/v4/projects/<gitlab-project-id>/packages/pypi/simple

## Usage

    simple_thermostat_ul --help 

## Development

    git clone gitlab.com/opensourcelab/simple-thermostat-unitelabs

    # create a virtual environment and activate it then run

    pip install -e .[dev]

    # run unittests

    invoke test   # use the invoke environment to manage development
    

## Documentation

The Documentation can be found here: [https://opensourcelab.gitlab.io/simple-thermostat-unitelabs](https://opensourcelab.gitlab.io/simple-thermostat-unitelabs) or [simple-thermostat-unitelabs.gitlab.io](simple_thermostat_ul.gitlab.io/)


## Credits

This package was created with [Cookiecutter](https://github.com/audreyr/cookiecutter)
 and the [gitlab.com/opensourcelab/software-dev/cookiecutter-pypackage](https://gitlab.com/opensourcelab/software-dev/cookiecutter-pypackage) project template.



